# Comparing booleans safely

Based on discussion, that n00bs tend to write

```java
if( a == b ){
    return true;
}else{
    return false;
}
```

instead of simply `return a == b;`, I decided to take the joke too far.


## Usage instructions

If you happen to have two booleans and you are interested in checking if they are equal, you can do following:

```java
boolean a = ...;
boolean b = ...;
boolean equal = BooleanEqualsHelper.isEqualAB(a,b);
```

Very simple! But there is more, you have methods up to 4 separate boolean parameters!
```java
boolean a = ...;
boolean b = ...;

boolean equal = BooleanEqualsHelper.isEqualAB(a,b);
```

```java
boolean a = ...;
boolean b = ...;
boolean c = ...;
boolean equal = BooleanEqualsHelper.isEqualABC(a,b,c);
```

```java
boolean a = ...;
boolean b = ...;
boolean c = ...;
boolean d = ...;
boolean equal = BooleanEqualsHelper.isEqualABCD(a,b,c,d);
```

## Advanced techniques

For really advanced users, there is a varargs method!

```java
boolean equal = BooleanEqualsHelper.isEqualMany(a,b,c,d,e,f,g,h,i);

boolean equal = BooleanEqualsHelper.isEqualMany(a,arrayOfBooleans);
```




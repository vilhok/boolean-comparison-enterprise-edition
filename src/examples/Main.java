package examples;

import exceptions.BooleanComparatorException;
import impl.BooleanEqualsHelper;

public class Main {

	public static void main(String[] args) {
		boolean a = true;
		boolean b = false;
		try {
			boolean c = BooleanEqualsHelper.isEqualAB(a, b);

			System.out.println("Result:" + c);
		} catch (BooleanComparatorException e) {
			e.printStackTrace();
		}
		
		try {
			boolean c = BooleanEqualsHelper.isEqualMany(a,true,true,false,true);

			System.out.println("Result:" + c);
		} catch (BooleanComparatorException e) {
			e.printStackTrace();
		}
	}

}

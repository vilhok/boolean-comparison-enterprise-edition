package builders;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;

import parameters.AbstractBooleanParameterContainer;
import util.StringCapitalizeFunctions;

public class BooleanParameterContainerBuilder {

	private static final String BOOLEAN_PARAMETER_CONTAINER_CLASSNAME_PATH = "parameters.";
	private static final String BOOLEAN_PARAMETER_CONTAINER_CLASSNAME_SUFFIX = "ParameterContainer";

	private Boolean value;

	public BooleanParameterContainerBuilder setValue(boolean b) {
		value = (Boolean) b;
		return this;
	}

	public AbstractBooleanParameterContainer build()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {

		final String classNamePrefix = StringCapitalizeFunctions.capitalizeFirstLetter(value);

		String className = BOOLEAN_PARAMETER_CONTAINER_CLASSNAME_PATH + classNamePrefix
				+ BOOLEAN_PARAMETER_CONTAINER_CLASSNAME_SUFFIX;

		Class<?> clazz = Class.forName(className);

		return (AbstractBooleanParameterContainer) clazz.getDeclaredConstructor().newInstance();
	}
}

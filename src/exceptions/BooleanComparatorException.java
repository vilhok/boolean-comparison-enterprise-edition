package exceptions;

public class BooleanComparatorException extends Exception {

	public BooleanComparatorException(Exception reason) {
		super(reason);
	}

	public BooleanComparatorException(String string) {
		super(string);
	}
}

package comparators;

import java.lang.reflect.InvocationTargetException;

import parameters.AbstractBooleanParameterContainer;

public abstract class AbstractBooleanComparator {

	public abstract AbstractBooleanComparisonResult isEqual(AbstractBooleanParameterContainer firstBoolean,
			AbstractBooleanParameterContainer secondBoolean)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException;

}

package comparators;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.Arrays;

import exceptions.BooleanComparatorIndexOutofBoundsException;
import parameters.AbstractBooleanParameterContainer;
import results.EqualsFalseComparisonResult;
import results.EqualsTrueComparisonResult;
import util.StringIndexVariables;

public class DefaultBooleanComparator extends AbstractBooleanComparator {

	private final AbstractBooleanParameterContainer[] values;

	int index = 0;

	public DefaultBooleanComparator(BigInteger count) {
		values = new AbstractBooleanParameterContainer[count.intValue()];

	}

	public void addParameterContainer(AbstractBooleanParameterContainer container)
			throws BooleanComparatorIndexOutofBoundsException {

		if (index == values.length) {
			throw new BooleanComparatorIndexOutofBoundsException();
		}

		values[index++] = container;
	}

	@Override
	public AbstractBooleanComparisonResult isEqual(AbstractBooleanParameterContainer firstBoolean,
			AbstractBooleanParameterContainer secondBoolean) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		AbstractBooleanParameterContainer previous = null;
		if (values[0] != null)
			previous = values[0];
		AbstractBooleanComparisonResult result = null;
		for (AbstractBooleanParameterContainer container : values) {
			if (previous != null) {
				if (container != null) {

					result = previous.getBooleanParameter().checkEqualBooleanValue(container.getBooleanParameter());

					if (result instanceof EqualsFalseComparisonResult) {
						return result;
					}
					previous = container;
				}
			}
		}
		if (firstBoolean != null) {
			if (previous != null) {
				result = previous.getBooleanParameter().checkEqualBooleanValue(firstBoolean.getBooleanParameter());
				if (result instanceof EqualsFalseComparisonResult) {
					return result;
				}
			}
			previous = firstBoolean;
		}

		if (secondBoolean != null) {
			if (previous != null) {
				result = previous.getBooleanParameter().checkEqualBooleanValue(secondBoolean.getBooleanParameter());
				if (result instanceof EqualsFalseComparisonResult) {
					return result;
				}
			}
		}

		return new EqualsTrueComparisonResult();
	}

}

package util;

public class StringCapitalizeFunctions {

	public static String capitalizeFirstLetter(Object value) {
		return value.toString()
				.substring(StringIndexVariables.STRING_BEGIN_INDEX, StringIndexVariables.STRING_SECOND_CHARACTER_INDEX)
				.toUpperCase() + value.toString().substring(StringIndexVariables.STRING_SECOND_CHARACTER_INDEX);
	}

}

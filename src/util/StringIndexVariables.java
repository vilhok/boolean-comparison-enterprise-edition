package util;

import java.math.BigInteger;

import comparators.AbstractBooleanComparisonResult;
import results.EqualsFalseComparisonResult;
import results.EqualsTrueComparisonResult;

public class StringIndexVariables {
	public static final Integer STRING_BEGIN_INDEX = BigInteger.ZERO.intValue();
	public static final Integer STRING_SECOND_CHARACTER_INDEX = BigInteger.ONE.intValue();
	

}

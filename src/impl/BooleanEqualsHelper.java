package impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;

import builders.BooleanParameterContainerBuilder;
import comparators.AbstractBooleanComparator;
import comparators.AbstractBooleanComparisonResult;
import comparators.DefaultBooleanComparator;
import exceptions.BooleanComparatorException;
import exceptions.BooleanComparatorIndexOutofBoundsException;
import factories.AbstractBooleanComparatorFactory;
import parameters.AbstractBooleanParameterContainer;
import parameters.FalseParameterContainer;
import parameters.TrueParameterContainer;
import results.EqualsFalseComparisonResult;
import results.EqualsTrueComparisonResult;

public class BooleanEqualsHelper {

	public static boolean isEqualAB(boolean a, boolean b) throws BooleanComparatorException {
		AbstractBooleanParameterContainer first;
		AbstractBooleanParameterContainer second;
		try {
			first = new BooleanParameterContainerBuilder().setValue(a).build();
			second = new BooleanParameterContainerBuilder().setValue(b).build();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new BooleanComparatorException(e1);
		}

		AbstractBooleanComparatorFactory abcf = AbstractBooleanComparatorFactory.getFactory();
		
		AbstractBooleanComparator comp;
		try {
			comp = abcf.getComparator(BigInteger.TWO, null);
		} catch (Exception e2) {
			throw new BooleanComparatorException(e2);
		}
		
		try {
			((DefaultBooleanComparator) comp).addParameterContainer(first);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		try {
			((DefaultBooleanComparator) comp).addParameterContainer(second);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		AbstractBooleanComparisonResult result;
		try {
			result = comp.isEqual(null, null);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new BooleanComparatorException(e);
		}

		if (result instanceof EqualsTrueComparisonResult) {
			return new TrueParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		} else if (result instanceof EqualsFalseComparisonResult) {
			return new FalseParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		}

		throw new BooleanComparatorException(
				"This line cannot be reached, but due to shitty code I have to handle the exception");
	}

	public static boolean isEqualABC(boolean a, boolean b, boolean c) throws BooleanComparatorException {
		AbstractBooleanParameterContainer first, second, third;
		try {
			first = new BooleanParameterContainerBuilder().setValue(a).build();
			second = new BooleanParameterContainerBuilder().setValue(b).build();
			third = new BooleanParameterContainerBuilder().setValue(c).build();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new BooleanComparatorException(e1);
		}
		AbstractBooleanComparatorFactory abcf = AbstractBooleanComparatorFactory.getFactory();
		AbstractBooleanComparator comp;
		try {
			comp = abcf.getComparator(BigInteger.TWO, null);
		} catch (Exception e2) {
			throw new BooleanComparatorException(e2);
		}
		try {
			((DefaultBooleanComparator) comp).addParameterContainer(first);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		try {
			((DefaultBooleanComparator) comp).addParameterContainer(second);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		AbstractBooleanComparisonResult result;
		try {
			result = comp.isEqual(third, null);// let's give the third here because
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {

			throw new BooleanComparatorException(e);
		} 

		if (result instanceof EqualsTrueComparisonResult) {
			return new TrueParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		} else if (result instanceof EqualsFalseComparisonResult) {
			return new FalseParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		}

		throw new BooleanComparatorException(
				"This line cannot be reached, but due to shitty code I have to handle the exception");
	}

	public static boolean isEqualABCD(boolean a, boolean b, boolean c, boolean d) throws BooleanComparatorException {
		AbstractBooleanParameterContainer first, second, third, fourth;
		try {
			first = new BooleanParameterContainerBuilder().setValue(a).build();
			second = new BooleanParameterContainerBuilder().setValue(b).build();
			third = new BooleanParameterContainerBuilder().setValue(c).build();
			fourth = new BooleanParameterContainerBuilder().setValue(d).build();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new BooleanComparatorException(e1);
		}

		AbstractBooleanComparatorFactory abcf = AbstractBooleanComparatorFactory.getFactory();
		AbstractBooleanComparator comp;
		try {
			comp = abcf.getComparator(BigInteger.TWO.add(BigInteger.ONE), null);
		} catch (Exception e2) {
			throw new BooleanComparatorException(e2);
		}

		try {
			((DefaultBooleanComparator) comp).addParameterContainer(first);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		try {
			((DefaultBooleanComparator) comp).addParameterContainer(second);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		try {
			((DefaultBooleanComparator) comp).addParameterContainer(third);
		} catch (BooleanComparatorIndexOutofBoundsException e) {
			Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
		}

		AbstractBooleanComparisonResult result;
		try {
			result = comp.isEqual(null, fourth);// let's give the fourth here because
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new BooleanComparatorException(e);
		} 

		if (result instanceof EqualsTrueComparisonResult) {
			return new TrueParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		} else if (result instanceof EqualsFalseComparisonResult) {
			return new FalseParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		}

		throw new BooleanComparatorException(
				"This line cannot be reached, but due to shitty code I have to handle the exception");
	}

	public static boolean isEqualMany(boolean a, boolean... b) throws BooleanComparatorException {
		if (b.length == BigInteger.ZERO.intValue()) {
			return new TrueParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		}

		AbstractBooleanParameterContainer first;
		try {
			first = new BooleanParameterContainerBuilder().setValue(a).build();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new BooleanComparatorException(e1);
		}
		AbstractBooleanComparatorFactory abcf = AbstractBooleanComparatorFactory.getFactory();

		AbstractBooleanComparator comp;
		try {
			comp = abcf.getComparator(BigInteger.valueOf(b.length), null);
		} catch (Exception e2) {
			throw new BooleanComparatorException(e2);
		}

		for (Boolean bool : b) {
			AbstractBooleanParameterContainer value;
			try {
				value = new BooleanParameterContainerBuilder().setValue(bool).build();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
				throw new BooleanComparatorException(e1);
			}
			try {
				((DefaultBooleanComparator) comp).addParameterContainer(value);
			} catch (BooleanComparatorIndexOutofBoundsException e) {
				Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
			}
		}
		AbstractBooleanComparisonResult result;
		try {
			result = comp.isEqual(first, null);// nothing makes sense
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new BooleanComparatorException(e);
		} 

		if (result instanceof EqualsTrueComparisonResult) {
			return new TrueParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		} else if (result instanceof EqualsFalseComparisonResult) {
			return new FalseParameterContainer().getBooleanParameter().getBooleanValue().booleanValue();
		}
		throw new BooleanComparatorException(
				"This line cannot be reached, but due to shitty code I have to handle the exception");
	}

}

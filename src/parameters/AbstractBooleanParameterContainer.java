package parameters;

public abstract class AbstractBooleanParameterContainer {

	public abstract AbstractBooleanParameter getBooleanParameter();
}

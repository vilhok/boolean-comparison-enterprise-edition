package parameters;

public class TrueParameterContainer extends BooleanParameterContainer{
	final AbstractBooleanParameter value = new TrueParameter();

	@Override
	public AbstractBooleanParameter getBooleanParameter() {
		return value;
	}
}

package parameters;

public class FalseParameterContainer extends BooleanParameterContainer{

	final AbstractBooleanParameter value = new FalseParameter();

	@Override
	public AbstractBooleanParameter getBooleanParameter() {
		return value;
	}
}

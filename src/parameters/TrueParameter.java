package parameters;

public class TrueParameter extends AbstractBooleanParameter {

	@Override
	public Boolean getBooleanValue() {
		return Boolean.TRUE;
	}
	
	
//	public boolean equals(Object other) {// ":D"
//		if(other.getClass() == this.getClass()) {// ":D"
//			return true;// ":D"
//		}else {// ":D"
//			return false;   // ":D"
//		}// ":D"
//	}
}
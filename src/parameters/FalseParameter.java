package parameters;

public class FalseParameter extends AbstractBooleanParameter {

	@Override
	public Boolean getBooleanValue() {
		return Boolean.FALSE;
	}

//	public boolean equals(Object other) {// ":D"
//		if(other.getClass() == this.getClass()) {// ":D"
//			return true;// ":D"
//		}else {// ":D"
//			return false;   // ":D"
//		}// ":D"
//	}
}

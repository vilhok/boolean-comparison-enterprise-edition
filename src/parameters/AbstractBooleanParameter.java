package parameters;

import java.lang.reflect.InvocationTargetException;

import comparators.AbstractBooleanComparisonResult;
import util.StringCapitalizeFunctions;

public abstract class AbstractBooleanParameter {

	private static final String a = "results.Equals";
	private static final String b = "ComparisonResult";

	public abstract Boolean getBooleanValue();

	public AbstractBooleanComparisonResult checkEqualBooleanValue(AbstractBooleanParameter booleanParameter) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		String resultname = StringCapitalizeFunctions
				.capitalizeFirstLetter(this.getClass().equals(booleanParameter.getClass()));

		String className = a + resultname + b;

		Class<?> clazz = Class.forName(className);

		return (AbstractBooleanComparisonResult) clazz.getDeclaredConstructor().newInstance();

	}
}

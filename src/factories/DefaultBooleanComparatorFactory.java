package factories;

import java.math.BigInteger;
import java.util.Comparator;

import comparators.AbstractBooleanComparator;
import comparators.DefaultBooleanComparator;
import parameters.AbstractBooleanParameterContainer;

public class DefaultBooleanComparatorFactory extends AbstractBooleanComparatorFactory {

	@Override
	public AbstractBooleanComparator getComparator(BigInteger count,
			Comparator<AbstractBooleanParameterContainer> object) {
		if (object != null) {
			throw new IllegalArgumentException("CUSTOM COMPARTORS NOT SUPPORTED");
		}
		return new DefaultBooleanComparator(count);
	}

}

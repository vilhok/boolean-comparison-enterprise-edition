package factories;

import java.math.BigInteger;
import java.util.Comparator;

import comparators.AbstractBooleanComparator;
import parameters.AbstractBooleanParameterContainer;

public abstract class AbstractBooleanComparatorFactory {

	public static AbstractBooleanComparatorFactory getFactory() {
		return new DefaultBooleanComparatorFactory();

	}

	public abstract AbstractBooleanComparator getComparator(BigInteger two,
			Comparator<AbstractBooleanParameterContainer> customComparator) throws Exception;
}
